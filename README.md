# Southern Utah Coronavirus Tracker

## Setup

    npm install

## Run

#### Development server

    npm run serve

#### Node server

    node index.js


## Build

    npm run build

> build files placed in dist/
