#!/usr/bin/env node
const express = require('express');
const cors = require('cors');
const csv = require("csvtojson");
const path = require('path');
const app = express();
const port = 3000;


app.use(cors());
app.use(express.static('public'));
app.use('/scripts', express.static(__dirname + '/node_modules/'));
express.static.mime.define({'application/javascript': ['js']});

app.get('/cases', (req, res) => {
    covid19File = 'logs/sw-ut-covid19.csv';
    csv().fromFile(covid19File)
        .then(data => {
            res.json(data);
        })
        .catch(() => {
            res.send(500);
        });
});


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

/*
app.get('/scripts/vue.js', (req, res) => {
    console.log('requesting vue...');
    res.sendFile(__dirname + '/node_modules/vue/dist/vue.min.js');
});
*/

app.listen(port, () => {
    console.log(`listening on port ${port}`);
});