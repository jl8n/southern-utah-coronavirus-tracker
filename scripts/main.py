from bs4 import BeautifulSoup
import requests
import datetime
import logging
import json
import twint
import json
import ast

            
def scrapeTwitter(filename) -> dict:
    c = twint.Config()
    c.Username = 'swuhealth'
    c.Since = '2020-3-6 00:00:00'  # date of first Utah case
    c.Search = 'COVID-19 Update'
    c.Store_json = True
    c.Output = filename
    twint.run.Search(c)

def processFile(infile, outfile):
    json_obj = {}
    json_arr = []

    with open(infile, 'r') as f:
        lines = f.readlines()
        
    for line in lines:
        json_arr.append(json.loads(line))
    
    json_obj['tweets'] = json_arr

    with open(outfile, 'w') as f:
        json.dump(json_obj, f, indent=4)


def parseFile(filename):
    with open(filename, 'r') as f:
        data = json.load(f)

    for tweet in data['tweets']:
        print(tweet['date'], tweet['tweet'])
        print('__________________________________________________________')

def scrapeHtml() -> dict:
    """Scrape html from the Soutwest Utah Public Health Department"""
    url = 'https://swuhealth.org/covid/'
    print('Pulling data from {}'.format(url))
    req = requests.get(url)
    soup = BeautifulSoup(req.content, "html.parser")

    # bless the web developer who made this page a joy to parse
    lastUpdated = soup.h4.text.lstrip('UPDATED')
    date = datetime.datetime.strptime(lastUpdated, ' %B %d, %Y')
    date = date.strftime('%Y-%m-%d ')

    parent = soup.find(class_='entry-content')
    block = parent.ul.li
    sw_ut_cases = block.select('strong')[2].get_text(strip=True)
    sw_ut_cases = int(sw_ut_cases)
    recovered = block.select('ul > li')[0].get_text(strip=True)
    recovered = int(recovered.rstrip('recovered'))
    new_cases = block.select('ul > li')[1].get_text(strip=True)
    new_cases = int(new_cases.rstrip('new cases'))
    hospitalized = block.select('ul > li')[2].get_text(strip=True)
    hospitalized = int(hospitalized.rstrip('currently hospitalized'))
    deaths = block.select('ul > li')[3].get_text(strip=True)
    deaths = int(deaths.split(' ')[0].rstrip('deaths'))
    wash_county = block.select('ul > li')[4].get_text(strip=True)
    wash_county = int(wash_county.split(' ')[2])
    tested = block.select('ul > li')[9].get_text(strip=True)
    tested = int(tested.split(' ')[0].replace(',', ''))
    #print(data['sw_ut_cases'], data['recovered'], data['new_cases'], data['hospitalized'], data['deaths'], data['wash_county'], data['tests'])

    data = {
        'date':date,
        'swUtahTotal': sw_ut_cases,
        'recovered': recovered,
        'new_cases': new_cases,
        'hospitalized': hospitalized,
        'deaths': deaths,
        'washgintonCounty': wash_county,
        'tested': tested
    }

    return data


def getLastLogTime(filename: str) -> datetime:
    """Return the time of the last logged event"""
    with open(filename, 'r') as fin:
        lines = fin.readlines()
        lastLine = lines[-1]
    
    date = lastLine.split()[0]
    return date


def logResults(data: dict, filename: str):
    """Appends all the values in a dict to a whitespace-separated file"""
    line = ''
    for value in data:
        line += str(data[value]) + ' '
    line += '\n'

    with open(filename, 'a') as fout:
        fout.write(line)


def programLog(filename: str, log_type: str, data: str):
    with open(filename, 'r') as fin:
        fileData = json.load(fin)
    
    fileData['programLogs'].append(data)

    with open(filename, 'w') as fout:
        json.dump(fileData, fout, indent=2)


def covidLog(data: dict, filename: str):
    print("DATA", data)
    with open(filename, 'r') as fin:
        fileData = json.load(fin)

    fileData['covidLogs'].append(data)

    with open(filename, 'w') as fout:
        json.dump(fileData, fout, indent=2)

    print(json.dumps(fileData['covidLogs'], indent=2))


def main():
    # file i/o & date parsing
    today = datetime.datetime.today().strftime('%Y-%m-%d')
    today = today.strip()
    data = scrapeHtml()
    covidLog(data, 'log.json')
    last_update = data['date'].rstrip()
    last_update = datetime.datetime.strptime(last_update, '%Y-%m-%d')
    last_log = getLastLogTime('log.log')
    last_log = datetime.datetime.strptime(last_log, '%Y-%m-%d')

    if last_log < last_update:  # site has refreshed
        print('Page has been updated. Logging data ...')
        logResults(data, 'log.log')
        #programLog('info', 'Updated!')
    elif last_log == last_update:
        #programLog('info', 'Page has not been updated')
        print('Page has not been updated since {}. Doing nothing.'.format(last_log, last_update))
    
    #programLog('info', )


if __name__ == '__main__':
    #main()
    results_file = 'results.txt'
    json_file = 'tweets.json'

    scrapeTwitter(results_file)
    processFile(results_file, json_file)
    parseFile(json_file)
