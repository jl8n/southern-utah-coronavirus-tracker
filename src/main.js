// When building our app
// the build tool will look at this file first to 
// figure out how to build the rest of the app

import Vue from 'vue';
import App from './App.vue';
import HighchartsVue from 'highcharts-vue'

Vue.use(HighchartsVue);

new Vue({
  render: h => h(App),
  template: '<App :data="data"></App>'
}).$mount('#app');